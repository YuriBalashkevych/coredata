//
//  AppDelegate.h
//  CoreData
//
//  Created by Yuri Balashkevych on 26.07.15.
//  Copyright (c) 2015 George. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

