//
//  BaseEntity.h
//  CoreData
//
//  Created by Yuri Balashkevych on 29.07.15.
//  Copyright (c) 2015 George. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BaseEntity : NSManagedObject

@property (nonatomic, retain) NSNumber *text;

+ (instancetype)createBaseEntityInContext:(NSManagedObjectContext *)moc;

@end
