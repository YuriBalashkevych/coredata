//
//  BaseEntity.m
//  CoreData
//
//  Created by Yuri Balashkevych on 29.07.15.
//  Copyright (c) 2015 George. All rights reserved.
//

#import "BaseEntity.h"


@implementation BaseEntity

@dynamic text;

#pragma mark - Creation

+ (instancetype)createBaseEntityInContext:(NSManagedObjectContext *)moc {
    
    id newObject = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
    return newObject;
}

@end
