//
//  DataManager.h
//  CoreData
//
//  Created by Yuri Balashkevych on 26.07.15.
//  Copyright (c) 2015 George. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *mainContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *backgroundContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *rootContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *distinctBackgroundContext;

@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveBackgroundContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)saveMainContextToPersistStoreAndWait:(BOOL)wait;
- (void)saveBackgroundContextToPersistStoreAndWait:(BOOL)wait;

+ (DataManager* )sharedManager;

@end
