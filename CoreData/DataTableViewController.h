//
//  DataTableViewController.h
//  CoreData
//
//  Created by Yuri Balashkevych on 29.07.15.
//  Copyright (c) 2015 George. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataTableViewController : UITableViewController

@end
