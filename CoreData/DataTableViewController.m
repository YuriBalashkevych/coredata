//
//  DataTableViewController.m
//  CoreData
//
//  Created by Yuri Balashkevych on 29.07.15.
//  Copyright (c) 2015 George. All rights reserved.
//

/*
 It’s time to post change context notification and get ready state for reloading tableView.
 
 Inserting:
 10000 el.
 FRC : 0.016 s
 Array : 0.080
 
 100000 el.
 FRC : 0.190 s
 Array : 0.950
 
 1000000 el.
 FRC : 2.247
 Array : 11.991
 So, it’s like 5 times difference. But if you want to do smth with tableView without animation, NSArray will help you - visually it works faster.*/

#import "DataTableViewController.h"
#import "DataManager.h"
#import "BaseEntity.h"

static BOOL usingFRC = NO;

@interface DataTableViewController ()<NSFetchedResultsControllerDelegate> {
    NSDate *_startDate;
}

@property (strong, nonatomic) DataManager                   *dataManager;
@property (strong, nonatomic) NSFetchedResultsController    *fetchResultController;
@property (strong, nonatomic) NSArray                       *fetchedObjects;

@end

@implementation DataTableViewController

#pragma mark - Life Cycle

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (!usingFRC) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchedNewObjects:) name:NSManagedObjectContextDidSaveNotification object:[self.dataManager backgroundContext]];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)dealloc {
    if (self) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (usingFRC) {
        return [[self.fetchResultController sections] count];
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (usingFRC) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchResultController sections] objectAtIndex:section];
        return [[sectionInfo objects] count];
    } else {
        return [self.fetchedObjects count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"BaseEntity";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    BaseEntity *baseEntity = nil;
    if (usingFRC) {
        baseEntity = [self.fetchResultController objectAtIndexPath:indexPath];
    } else {
        baseEntity = [self.fetchedObjects objectAtIndex:indexPath.row];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%@",baseEntity.text];
    return cell;
}

#pragma mark - NSFetchedResultsController

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    if (type == NSFetchedResultsChangeInsert) {
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self stopTimer];
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.tableView beginUpdates];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.tableView beginUpdates];
        });
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.tableView endUpdates];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView endUpdates];
        });
    }
}

- (NSString *)controller:(NSFetchedResultsController *)controller sectionIndexTitleForSectionName:(NSString *)sectionName {
    return nil;
}


#pragma mark - Actions

- (IBAction)addNewCell:(UIBarButtonItem *)sender {
    NSUInteger objCount;
    if (usingFRC) {
        NSFetchedResultsController *frc = self.fetchResultController;
        objCount = [[frc fetchedObjects] count];
    } else {
        objCount = [self.fetchedObjects count];
    }
    NSManagedObjectContext *bgContext = [self.dataManager backgroundContext];
    [bgContext performBlock:^{
        for (int i = (int)objCount; i < objCount + 10000; i++) {
            BaseEntity *newEntity = [BaseEntity createBaseEntityInContext:bgContext];
            newEntity.text = @(i);
        }
        if (!usingFRC) {
            [self.dataManager saveBackgroundContext];
        }
        [self startTimer];
    }];
}

#pragma mark - Lazy Instatiation

- (DataManager *)dataManager {
    if (!_dataManager) {
        _dataManager = [DataManager sharedManager];
    }
    return _dataManager;
}

- (NSFetchedResultsController *)fetchResultController {
    if (!_fetchResultController && usingFRC) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([BaseEntity class])];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"text" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        _fetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                     managedObjectContext:self.dataManager.backgroundContext
                                                                       sectionNameKeyPath:nil
                                                                                cacheName:nil];
        _fetchResultController.delegate = self;
        NSError *error = nil;
        if (![_fetchResultController performFetch:&error]) {
            NSLog(@"%@",[error localizedDescription]);
        }
    }
    return _fetchResultController;
}

#pragma mark - Traditional Array Fetch

- (void)fetchedNewObjects:(NSNotification *)notification {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"text" ascending:NO];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([BaseEntity class])];
    request.sortDescriptors = @[sortDescriptor];
    NSManagedObjectContext *bgMOC = [[DataManager sharedManager] backgroundContext];
    NSError *error = nil;
    self.fetchedObjects = [bgMOC executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    } else {
        [self stopTimer];
        if (![NSThread isMainThread]) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    }
}

#pragma mark - Check Run Time

- (void)startTimer {
    _startDate = [NSDate date];
}

- (void)stopTimer {
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_startDate];
    NSLog(@"%f seconds", interval);
}

@end


























